import os
from pathlib import Path
from setuptools import setup
from setuptools import Extension as BaseExtension
from setuptools.command.build_ext import build_ext
import shlex
import shutil
import subprocess as sp
import sys
import warnings

class CustomExtBuilder(build_ext):

    def build_extension(self, ext):
        if isinstance(ext, Precompiled):
            return ext.copy_precompiled(self)
        return super().build_extension(ext)


class Precompiled(BaseExtension):

    def __init__(self, name, precompiled, *args, **kwargs):
        super().__init__(name, [], *args, **kwargs)
        self.precompiled = Path(precompiled)

        
    def copy_precompiled(self, builder):
        if self.precompiled.exists():
            path = Path(builder.get_ext_fullpath(self.name))
            path.parent.mkdir(parents=True)
            shutil.copyfile(
                str(self.precompiled),
                builder.get_ext_fullpath(self.name)
            )
        else:
            print(f"Error: specified file {self.precompiled} not found!", file=sys.stderr)


def main():

    setup(
        name = 'xglab_dante',
        version = '3.7.26',
        description = """Linux-only, pre-compiled Python 3.7 library for controlling XGLab Dante""",
        ext_modules=[
            Precompiled(
                "XGL_DPP",
                precompiled="lib/libXGL_DPP.so.3.7"
            ),
        ],
        cmdclass={"build_ext": CustomExtBuilder},
    )

if __name__ == '__main__':
    main()

